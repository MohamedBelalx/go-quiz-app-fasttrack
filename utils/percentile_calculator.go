// utils/percentile_calculator.go
package utils

import (
	"fmt"
	"sort"
)

// CalculatePercentile calculates the percentile for a given score and total scores.
func CalculatePercentile(userScore int, allScores []int) float64 {
	sort.Ints(allScores)

	fmt.Println("--------------")
	fmt.Println(userScore)
	fmt.Println(allScores)
	fmt.Println("--------------")

	var usersBelow int
	for _, score := range allScores {
		if score <= userScore {
			usersBelow++
		}
	}

	totalUsers := len(allScores)
	percentile := (float64(usersBelow) / float64(totalUsers)) * 100

	fmt.Println(percentile)
	return percentile
}
