// utils/score_calculator.go
package utils

import "quiz-app/models"

func CalculateScore(answers []int) int {
	correctAnswers := 0
	for i, answer := range answers {
		if answer == models.Questions[i].Correct {
			correctAnswers++
		}
	}
	return correctAnswers
}

// CalculateRanking calculates the user's rank based on their score and all scores.
func CalculateRanking(userScore int, allScores []int) int {
	rank := 1
	for _, score := range allScores {
		if userScore < score {
			rank++
		}
	}
	return rank
}
