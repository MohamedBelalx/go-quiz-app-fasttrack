# Quiz App

The Quiz App is a simple command-line and REST API quiz application built in Go. It allows users to take a quiz, submit their answers, and view their ranking and percentile compared to other quizzers.

## Features

- Take a quiz with multiple-choice questions.
- Submit your answers and receive a score.
- View your ranking and percentile on the leaderboard.
- Compare your performance with other quizzers.

## Requirements

- Go 1.20 or higher

## Getting Started


### 1. Clone the repository:
```sh
git clone https://github.com/yourusername/quiz-app.git
cd quiz-app
```
### 2. Build and run the application:
```sh
go run main.go
```
### 3. Use the CLI to take the quiz:
```sh
go run cli/main.go submit
```
### 4. Submit your answers using the CLI:

### 5. Alternatively, interact with the REST API:

- Get quiz questions: `GET http://localhost:8080/questions`
- Submit answers: `POST http://localhost:8080/submit`
- View leaderboard: `GET http://localhost:8080/leaderboard`

## Project Structure

- `main.go`: Application entry point.
- `app/`: Contains server setup logic.
- `api/`: Handles API routes and request handling.
- `models/`: Defines data models for questions and scores.
- `utils/`: Contains utility functions for score calculation and percentile calculation.
