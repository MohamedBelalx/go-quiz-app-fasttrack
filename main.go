// main.go
package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"quiz-app/api"
	"quiz-app/models"
)

func main() {
	models.InitScoresDB()

	router := gin.Default()
	router.GET("/questions", api.GetQuestions)
	router.POST("/submit", api.SubmitAnswers)
	router.GET("/leaderboard", api.GetLeaderboard)

	fmt.Println("Server listening on :8080")
	router.Run(":8080")
}
