// api/quiz_handler.go
package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"quiz-app/models"
	"quiz-app/utils"
)

func GetQuestions(c *gin.Context) {
	c.JSON(http.StatusOK, models.Questions)
}

func SubmitAnswers(c *gin.Context) {
	var userAnswers []int
	if err := c.ShouldBindJSON(&userAnswers); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request"})
		return
	}

	userScore := utils.CalculateScore(userAnswers)
	models.ScoresDB.AddScore(userScore)

	c.JSON(http.StatusOK, gin.H{"score": userScore})
}

func GetLeaderboard(c *gin.Context) {
	userAnswers := make([]int, len(models.Questions))
	if err := c.ShouldBindJSON(&userAnswers); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request"})
		return
	}

	fmt.Println(userAnswers)
	userScore := utils.CalculateScore(userAnswers)

	allScores := models.ScoresDB.GetAllScores()

	fmt.Println("user Score")
	fmt.Println(userScore)
	userRank := utils.CalculateRanking(userScore, allScores)
	userPercentile := utils.CalculatePercentile(userScore, allScores)

	c.JSON(http.StatusOK, gin.H{"user_rank": userRank, "user_percentile": userPercentile})
}
