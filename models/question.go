// models/question.go
package models

type Question struct {
	ID      int
	Text    string
	Options []string
	Correct int // Index of the correct option
}

var Questions = []Question{
	{
		Text:    "What is the capital of France?",
		Options: []string{"London", "Paris", "Berlin", "Madrid"},
		Correct: 1, // Paris is the correct answer
	},
	{
		Text:    "Which planet is known as the Red Planet?",
		Options: []string{"Mars", "Venus", "Jupiter", "Mercury"},
		Correct: 0, // Mars is the correct answer
	},
	{
		Text:    "What is the largest mammal?",
		Options: []string{"Elephant", "Blue Whale", "Giraffe", "Hippopotamus"},
		Correct: 1, // Blue Whale is the correct answer
	},
	{
		Text:    "Which gas do plants use for photosynthesis?",
		Options: []string{"Oxygen", "Carbon Dioxide", "Nitrogen", "Hydrogen"},
		Correct: 1, // Carbon Dioxide is the correct answer
	},
	{
		Text:    "Who painted the Mona Lisa?",
		Options: []string{"Pablo Picasso", "Vincent van Gogh", "Leonardo da Vinci", "Michelangelo"},
		Correct: 2, // Leonardo da Vinci is the correct answer
	},
	// Add more questions here
}

type QuizResult struct {
	UserAnswers []int
	Score       int
}
