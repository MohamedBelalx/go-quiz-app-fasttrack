// models/score.go
package models

type ScoreDatabase struct {
	Scores []int
}

var ScoresDB ScoreDatabase

func InitScoresDB() {
	ScoresDB = ScoreDatabase{}
}

func (db *ScoreDatabase) AddScore(score int) {
	db.Scores = append(db.Scores, score)
}

func (db *ScoreDatabase) GetAllScores() []int {
	return db.Scores
}
