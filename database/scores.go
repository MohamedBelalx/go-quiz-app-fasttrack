// database/scores.go
package database

import (
	"sync"
)

var scores []int
var scoresMutex sync.RWMutex

func InitScores() {
	scores = []int{}
}

func AddScore(score int) {
	scoresMutex.Lock()
	defer scoresMutex.Unlock()

	scores = append(scores, score)
}

func GetAllScores() []int {
	scoresMutex.RLock()
	defer scoresMutex.RUnlock()

	return scores
}
