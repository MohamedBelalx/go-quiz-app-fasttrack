// cli/main.go
package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"quiz-app/cli/cmd"
)

func main() {
	var rootCmd = &cobra.Command{Use: "quizapp"}
	rootCmd.AddCommand(cmd.FetchCmd)   // Update this line
	rootCmd.AddCommand(cmd.SubmitCmd)  // Update this line

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
