// cli/cmd/fetch_questions.go
package cmd

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/spf13/cobra"
	"quiz-app/models"
)

var FetchCmd = &cobra.Command{
	Use:   "fetch",
	Short: "Fetch quiz questions",
	Run:   fetchQuestions,
}

func fetchQuestions(cmd *cobra.Command, args []string) {
	resp, err := http.Get("http://localhost:8080/questions")
	if err != nil {
		fmt.Println("Error fetching questions:", err)
		return
	}
	defer resp.Body.Close()

	var questions []models.Question
	err = json.NewDecoder(resp.Body).Decode(&questions)
	if err != nil {
		fmt.Println("Error decoding response:", err)
		return
	}

	for i, question := range questions {
		fmt.Println("Question:", question.Text)
		for j, option := range question.Options {
			fmt.Printf("  %d. %s\n", j+1, option)
		}

		// Display a prompt and read user input
		var userInput string
		fmt.Print("Your answer (enter the option number, or 'q' to quit): ")
		fmt.Scanln(&userInput)
		if userInput == "q" {
			fmt.Println("Exiting quiz...")
			return
		}

		answer, err := strconv.Atoi(userInput)
		if err != nil || answer <= 0 || answer > len(question.Options) {
			fmt.Println("Invalid answer, please try again.")
			i-- // Re-ask the same question
			continue
		}
	}
}
