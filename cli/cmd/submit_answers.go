// cli/cmd/submit_answers.go
package cmd

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"quiz-app/models"
)

var SubmitCmd = &cobra.Command{
	Use:   "submit",
	Short: "Submit quiz answers",
	Run:   submitAnswers,
}

func init() {
	// Initialize Cobra command and flags here
}

func submitAnswers(cmd *cobra.Command, args []string) {
	reader := bufio.NewReader(os.Stdin)

	answers := make([]int, len(models.Questions))

	for i, question := range models.Questions {
		fmt.Println("Question:", question.Text)
		for j, option := range question.Options {
			fmt.Printf("  %d. %s\n", j+1, option)
		}

		fmt.Print("Your answer (enter the option number): ")
		answerStr, _ := reader.ReadString('\n')
		answerStr = strings.TrimSpace(answerStr)
		answer, err := strconv.Atoi(answerStr)
		if err != nil || answer <= 0 || answer > len(question.Options) {
			fmt.Println("Invalid answer, please try again.")
			i-- // Re-ask the same question
			continue
		}
		answers[i] = answer - 1
	}

	// Submit answers using HTTP request
	submitAnswersHTTP(answers)
	getLeaderboardHTTP(answers)
}

func submitAnswersHTTP(answers []int) {
	url := "http://localhost:8080/submit"
	data, _ := json.Marshal(answers)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		fmt.Println("Error submitting answers:", err)
		return
	}
	defer resp.Body.Close()

	var response struct {
		Score int
	}
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		fmt.Println("Error decoding response:", err)
		return
	}

	fmt.Printf("Your score: %d out of %d\n", response.Score, len(models.Questions))
}

func getLeaderboardHTTP(answers []int) {
	url := "http://localhost:8080/leaderboard"
	data, _ := json.Marshal(answers)
	fmt.Println(data)
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Error fetching leaderboard:", err)
		return
	}
	defer resp.Body.Close()

	var leaderboard struct {
		UserRank       int
		UserPercentile float64
	}

	err = json.NewDecoder(resp.Body).Decode(&leaderboard)
	if err != nil {
		fmt.Println("Error decoding leaderboard response:", err)
		return
	}
	fmt.Println("***********************")
	fmt.Println(leaderboard)
	fmt.Printf("You were better than %v%% of all quizzers\n", leaderboard.UserPercentile)
}
